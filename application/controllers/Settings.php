<?php defined('BASEPATH') OR exit('No direct script access allowed');

class settings extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
	}
	public function index() {
		$this->load->view('setting/setting', $this->data);
	}
}