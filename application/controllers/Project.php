<?php defined('BASEPATH') OR exit('No direct script access allowed');

class project extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
	}
	public function add() {
		$this->load->view('project/add', $this->data);
	}
	public function list() {
		$this->load->view('project/list', $this->data);
	}
}