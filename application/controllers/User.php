<?php defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
	}
	public function login() {
		$this->load->view('user/login', $this->data);
	}
	public function register() {
		$this->load->view('user/register', $this->data);
	}
	public function forgot() {
		$this->load->view('user/forgot', $this->data);
	}
	public function profile() {
		$this->load->view('user/profile', $this->data);
	}
	public function logout() {
	}
}