<?php defined('BASEPATH') OR exit('No direct script access allowed');

class generators extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
	}
	public function quotation() {
		$this->load->view('generator/quotation', $this->data);
	}
	public function invoice() {
		$this->load->view('generator/invoice', $this->data);
	}
	public function receipt() {
		$this->load->view('generator/receipt', $this->data);
	}
	public function techspecs() {
		$this->load->view('generator/techspecs', $this->data);
	}
	public function uat() {
		$this->load->view('generator/uat', $this->data);
	}
	public function agreement() {
		$this->load->view('generator/agreement', $this->data);
	}
}