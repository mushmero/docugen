<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">
	<title>Document Generator</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('assets/images/docugen.png'); ?>" type="image/x-icon">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/node-waves/waves.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/animate-css/animate.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/dropzone/dropzone.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/themes/all-themes.css'); ?>">
</head>