<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/momentjs/moment.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-notify/bootstrap-notify.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/node-waves/waves.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-countto/jquery.countTo.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-steps/jquery.steps.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/sweetalert/sweetalert.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/autosize/autosize.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dropzone/dropzone.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
</body>
</html>