<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>">DocuGen</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->