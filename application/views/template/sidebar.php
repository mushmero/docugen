<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(1);?>

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?php echo base_url('assets/images/user2.jpg'); ?>" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Amirul Hakim</div>
                <div class="email">amirulhakimzailan@gmail.com</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?php echo site_url('user/profile'); ?>"><i class="material-icons">person</i>Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="<?php echo site_url('logout'); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <?php if ($uri == 'dashboard') {?>
                <li class="active">
                <?php } else {?>
                <li>
                <?php }?>
                    <a href="<?php echo site_url('dashboard'); ?>">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php if ($uri == 'project') {?>
                <li class="active">
                <?php } else {?>
                <li>
                <?php }?>
                    <a href="javascript:void(0);" class="menu-toggle">
                		<i class="material-icons">work</i>
                		<span>Projects</span>
                	</a>
                	<ul class="ml-menu">
                		<li>
                			<a href="<?php echo site_url('project/add'); ?>">Add New</a>
                		</li>
                		<li>
                			<a href="<?php echo site_url('project/list'); ?>">List</a>
                		</li>
                	</ul>
                </li>
                <?php if ($uri == 'generators') {?>
                <li class="active">
                <?php } else {?>
                <li>
                <?php }?>
                	<a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">build</i>
                        <span>Generator</span>
                    </a>
                    <ul class="ml-menu">
                    	<li>
                    		<a href="<?php echo site_url('generators/quotation'); ?>">Quotation</a>
                    	</li>
                    	<li>
                    		<a href="<?php echo site_url('generators/invoice'); ?>">Invoice</a>
                    	</li>
                    	<li>
                    		<a href="<?php echo site_url('generators/receipt'); ?>">Receipt</a>
                    	</li>
                    	<li>
                    		<a href="<?php echo site_url('generators/techspecs'); ?>">Tech Specs</a>
                    	</li>
                    	<li>
                    		<a href="<?php echo site_url('generators/uat'); ?>">UAT/Manual</a>
                    	</li>
                    	<li>
                    		<a href="<?php echo site_url('generators/agreement'); ?>">Agreement</a>
                    	</li>
                    </ul>
                </li>
                <?php if ($uri == 'document') {?>
                <li class="active">
                <?php } else {?>
                <li>
                <?php }?>
                	<a href="#">
                		<i class="material-icons">folder</i>
                		<span>Documents</span>
                	</a>
                </li>
                <?php if ($uri == 'setting') {?>
                <li class="active">
                <?php } else {?>
                <li>
                <?php }?>
                	<a href="<?php echo site_url('setting'); ?>">
                		<i class="material-icons">settings</i>
                		<span>Setting</span>
                	</a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; <?php echo date('Y'); ?> DocuGen by <a href="http://mushmero.com">mushmero</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>