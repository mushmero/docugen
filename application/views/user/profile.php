<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
             <ol class="breadcrumb">
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">people</i> User
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">account_circle</i> Profile
                </li>
            </ol>
        </div>

        <!-- Content -->
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header bg-teal">
                        <h2>PROFILE</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <ul class="nav nav-tabs tab-nav-right tab-col-pink" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#profile_animation_1" data-toggle="tab">
                                            <i class="material-icons">account_circle</i> Profile
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#image_animation_1" data-toggle="tab">
                                            <i class="material-icons">insert_photo</i> Photo
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#password_animation_1" data-toggle="tab">
                                            <i class="material-icons">lock</i> Password
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane animated lightSpeedIn active" id="profile_animation_1">
                                        <form action="#" method="POST">
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">person</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">details</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Username">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">phone</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Phone">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">email</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Email">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix js-sweetalert">
                                                <div class="col-xs-12">
                                                    <div class="input-group">
                                                        <button type="submit" class="btn btn-block waves-effect bg-pink" data-type="success">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane animated lightSpeedIn" id="image_animation_1">
                                        <div class="row clearfix">
                                            <div class="col-xs-12 col-sm-4 col-md-4">&nbsp;</div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <a href="javascript:void(0);" class="thumbnail">
                                                    <img src="<?php echo base_url('assets/images/user2.jpg'); ?>" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">&nbsp;</div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-xs-12">
                                                <form action="#" id="frmFileUpload" class="dropzone" method="post" enctype="multipart/form-data">
                                                    <div class="dz-message">
                                                        <div class="drag-icon-cph">
                                                            <i class="material-icons">touch_app</i>
                                                        </div>
                                                        <h3>Drop files here or click to upload.</h3>
                                                    </div>
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple />
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane animated lightSpeedIn" id="password_animation_1">
                                        <form action="#" method="POST">
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">lock</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Current Password">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">lock</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="New Password">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="input-group input-group-sm">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">lock</i>
                                                        </span>
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Retype Password">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix js-sweetalert">
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <button type="submit" class="btn btn-block waves-effect bg-pink" data-type="success">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Content -->
    </div>
</section>

<?php $this->load->view('template/footer');?>