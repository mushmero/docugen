<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
             <ol class="breadcrumb">
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">settings</i> Setting
                </li>
            </ol>
        </div>

        <!-- Content -->
        <div class="row clearfix">
        	<div class="col-xs-12">
        		<div class="card">
        			<div class="header bg-grey">
        				<h2>Setting</h2>
        			</div>
        			<div class="body">
        				<div class="row clearfix">
        					<div class="col-xs-12 col-md-6">
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">
                                                <i class="material-icons"></i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Full Name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        						<div class="row clearfix">
        							<div class="col-xs-12">
        								<div class="input-group input-group-sm">
        									<span class="input-group-addon">
        										<i class="material-icons"></i>
        									</span>
        									<div class="form-line">
        										<input type="text" class="form-control" placeholder="Personal Address">
        									</div>
        								</div>
        							</div>
        						</div>
        						<div class="row clearfix">
        							<div class="col-xs-12">
        								<div class="input-group input-group-sm">
        									<span class="input-group-addon">
        										<i class="material-icons"></i>
        									</span>
        									<div class="form-line">
        										<input type="text" class="form-control" placeholder="Personal Account No">
        									</div>
        								</div>
        							</div>
        						</div>
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">
                                                <i class="material-icons"></i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Personal Phone No">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">
                                                <i class="material-icons"></i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Personal Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        					</div>
        					<div class="col-xs-12 col-md-6">
        						<div class="row clearfix">
        							<div class="col-xs-12">
        								<div class="input-group input-group-sm">
        									<span class="input-group-addon">
        										<i class="material-icons"></i>
        									</span>
        									<div class="form-line">
        										<input type="text" class="form-control" placeholder="Company Name">
        									</div>
        								</div>
        							</div>
        						</div>
        						<div class="row clearfix">
        							<div class="col-xs-12">
        								<div class="input-group input-group-sm">
        									<span class="input-group-addon">
        										<i class="material-icons"></i>
        									</span>
        									<div class="form-line">
        										<input type="text" class="form-control" placeholder="Company Address">
        									</div>
        								</div>
        							</div>
        						</div>
        						<div class="row clearfix">
        							<div class="col-xs-12">
        								<div class="input-group input-group-sm">
        									<span class="input-group-addon">
        										<i class="material-icons"></i>
        									</span>
        									<div class="form-line">
        										<input type="text" class="form-control" placeholder="Company Account No">
        									</div>
        								</div>
        							</div>
        						</div>
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">
                                                <i class="material-icons"></i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Company Phone No">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">
                                                <i class="material-icons"></i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Company Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        					</div>
        				</div>
        				<div class="row clearfix js-sweetalert">
        					<div class="col-xs-12">
        						<div class="input-group">
        							<button type="submit" class="btn btn-flat btn-block waves-effect bg-blue" data-type="success">Update</button>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End of Content -->
    </div>
</section>

<?php $this->load->view('template/footer');?>