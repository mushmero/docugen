<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
             <ol class="breadcrumb">
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">build</i> Generator
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">insert_drive_files</i> Tech Specs
                </li>
            </ol>
        </div>

        <!-- Content -->
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header bg-amber">
                        <h2>ADD NEW TECH SPECS</h2>
                    </div>
                    <div class="body"></div>
                </div>
            </div>
        </div>
        <!-- End of Content -->
    </div>
</section>

<?php $this->load->view('template/footer');?>