<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
             <ol class="breadcrumb">
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">build</i> Generator
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">insert_drive_files</i> Quotation
                </li>
            </ol>
        </div>

        <!-- Content -->
        <div class="row clearfix">
        	<div class="col-xs-12">
        		<div class="card">
        			<div class="header bg-amber">
        				<h2>GENERATE QUOTATION</h2>
        			</div>
        			<div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-6">&nbsp;</div>
                            <div class="col-xs-6">
                                <h2 class="pull-right"><span class="label bg-cyan">QUOTATION</span></h2>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">&nbsp;</div>
                            <div class="col-xs-12 col-sm-6">
                               <div class="form-group form-float">
                                    <label class="form-label">Billed From</label>
                                    <select class="form-control show-tick">
                                        <option value="">-- Please select --</option>
                                        <option value="1">Personal</option>
                                        <option value="2">Company</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">&nbsp;</div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">&nbsp;</div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group form-float">
                                    <label class="form-label">Project Details</label>
                                    <select class="form-control show-tick">
                                        <option value="">-- Please select --</option>
                                        <option value="1">AAA</option>
                                        <option value="2">BBB</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">&nbsp;</div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">&nbsp;</div>
                            <div class="col-xs-12 col-sm-3">
                                <button class="btn btn-flat btn-block bg-light-blue waves-effect" data-toggle="modal" data-target="#previewModal">Preview</button>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <button class="btn btn-flat btn-block bg-blue waves-effect">Generate</button>
                            </div>
                            <div class="col-sm-3">&nbsp;</div>
                        </div>
                        <!-- Preview Modal -->
                        <div class="modal fade" id="previewModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="largeModalLabel">Quotation - </h4>
                                    </div>
                                    <div class="modal-body">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sodales orci ante, sed ornare eros vestibulum ut. Ut accumsan
                                        vitae eros sit amet tristique. Nullam scelerisque nunc enim, non dignissim nibh faucibus ullamcorper.
                                        Fusce pulvinar libero vel ligula iaculis ullamcorper. Integer dapibus, mi ac tempor varius, purus
                                        nibh mattis erat, vitae porta nunc nisi non tellus. Vivamus mollis ante non massa egestas fringilla.
                                        Vestibulum egestas consectetur nunc at ultricies. Morbi quis consectetur nunc.
                                    </div>
                                    <div class="modal-footer">
                                        <a href="javascript:void(0);" class="btn btn-flat btn-block bg-red" data-dismiss="modal"><i class="material-icons">cancel</i> <span>Close</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of Preview Modal -->
                    </div>
        		</div>
        	</div>
        </div>
        <!-- End of Content -->
    </div>
</section>

<?php $this->load->view('template/footer');?>