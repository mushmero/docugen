<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                 <ol class="breadcrumb">
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons">home</i> Home
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons">work</i> Project
                        </a>
                    </li>
                    <li class="active">
                        <i class="material-icons">list</i> List
                    </li>
                </ol>
            </div>
        </div>
        <!-- Content -->
        <div class="row clearfix">
        	<div class="col-xs-12">
        		<div class="card">
        			<div class="header bg-orange">
        				<h2>PROJECT LIST</h2>
        			</div>
        			<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover basic-datatable dataTable">
								<thead>
									<tr>
										<td>#</td>
										<td>Project Details</td>
                                        <td>Client Details</td>
                                        <td>Date</td>
                                        <td>Status</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Cloud</td>
                                        <td>ABC Pt Ltd</td>
                                        <td>12/12/18</td>
                                        <td>New</td>
									</tr>
								</tbody>
							</table>
						</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- End of Content -->
    </section>

<?php $this->load->view('template/footer');?>