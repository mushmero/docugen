<?php $this->load->view('template/header');?>
<body class="theme-red">
<?php $this->load->view('template/page_loader');?>
<?php $this->load->view('template/topbar');?>
<?php $this->load->view('template/sidebar');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
             <ol class="breadcrumb">
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <i class="material-icons">work</i> Project
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">add</i> Add New
                </li>
            </ol>
        </div>

        <!-- Content -->
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header bg-orange">
                        <h2>NEW PROJECT</h2>
                    </div>
                    <div class="body">
                        <div class="controls">
                            <form method="POST" action="">
                                <h3>Project Details</h3>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="projectName" class="form-control" >
                                        <label class="form-label">Project Name *</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="startDate" class="datepicker form-control" placeholder="Start Date *" >
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="endDate" class="datepicker form-control" placeholder="End Date *" >
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea rows="1" class="form-control no-resize auto-growth" name="projectDesc" placeholder="Project Description"></textarea>
                                    </div>
                                </div>
                                <h3>Client Details</h3>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="clientName" class="form-control" >
                                        <label class="form-label">Client Name *</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="clientCompany" class="form-control" >
                                        <label class="form-label">Client Company Name *</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="clientAddress" class="form-control" >
                                        <label class="form-label">Client Address *</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="clientPhone" class="form-control" >
                                        <label class="form-label">Client Phone No *</label>
                                    </div>
                                </div>
                                <h3>Job Details</h3>
                                <div class="entry">
                                    <div class="row clearfix">
                                        <div class="col-xs-10 col-sm-11">
                                            <div class="row clearfix">
                                                <div class="col-xs-12 col-sm-7">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="jobDetails[]" class="form-control" >
                                                            <label class="form-label">Job Details</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-2">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="quantity[]" class="form-control" >
                                                            <label class="form-label">Quantity</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-2">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" name="price[]" class="form-control" >
                                                            <label class="form-label">Unit Price(RM)</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-sm-1">
                                            <button type="button" class="btn btn-circle waves-circle bg-green btn-add">
                                                <i class="material-icons">add</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of entry class -->                                   
                            </form>
                                <div class="row">
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-xs-12 col-sm-4">
                                        <button type="" class="btn btn-flat btn-block bg-blue waves-effect">Submit</button>
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                </div> 
                        </div>
                        <!-- end of controls -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Content -->
    </div>
</section>

<?php $this->load->view('template/footer');?>